﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "EnttModuleBase.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class UNREALENTT_API UEnttModuleBase : public UObject
{
	GENERATED_BODY()

public:

	virtual void Initialize()
	{
		
	}
};
