// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Algo/Transform.h"
#include "ECS/Entities/Entities.h"
#include "Data/InitContext.h"
#include "SmallSystems.h"
#include "Data/GameData.h"
#include "ECS/Others/GameMath.h"
#include "ECS/Resources/Resources.h"
#include "EnttIntegration/EnttWorld.h"
#include "Scene/MapController.h"
#include "Engine/World.h"
#include "Interfaces/TextViewerWidget.h"
#include "Scene/UnitController.h"

namespace RadMag
{
	//Work with resource

	FORCEINLINE auto GetCreateGameRulesCommand(const FCreateGameRulesInitContext& Context) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([Context](FEnttData& EnttData)
		{
			if (EnttData.ResourceIsCreated<FGameRules>())
				return ESystemStatus::Failed;

			EnttData.CreateResource(FGameRules(Context.MapLength, Context.MapHeight));
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreateRenderRulesCommand() -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([](FEnttData& EnttData)
		{
			if (EnttData.ResourceIsCreated<FRenderRules>())
				return ESystemStatus::Failed;

			EnttData.CreateResource(FRenderRules());
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreateResourceListCommand() -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([](FEnttData& EnttData)
		{
			if (EnttData.ResourceIsCreated<FResourceList>())
				return ESystemStatus::Failed;

			EnttData.CreateResource(FResourceList());
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreateResourceCommand(const FCreateResourceInitContext& Context) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([Context](FEnttData& EnttData)
		{
			if (!EnttData.ResourceIsCreated<FResourceList>())
				return ESystemStatus::Failed;

			auto& ResourceList = EnttData.GetResource<FResourceList>();
			const auto Name = CEntityName(Context.Name);
			if (ResourceList.Contains(Name))
				return ESystemStatus::Failed;

			ResourceList.Add(CResource(Name, 0));
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreateFactoryListCommand() -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([](FEnttData& EnttData)
		{
			if (EnttData.ResourceIsCreated<FFactoryList>())
				return ESystemStatus::Failed;

			EnttData.CreateResource(FFactoryList());
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreateFactoryCommand(const FCreateFactoryInitContext& Context) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([Context](FEnttData& EnttData)
		{
			auto FactoryInput = Context.FactoryInput;
			auto FactoryOutput = Context.FactoryOutput;
			auto& FactoryList = EnttData.GetResource<FFactoryList>();
			if (FactoryList.Contains(CEntityName(Context.Name))
				|| !IsCorrectResourceMap(FactoryInput, EnttData)
				|| !IsCorrectResourceMap(FactoryOutput, EnttData))
				return ESystemStatus::Failed;

			TArray<CResource> BuildingInputMap;
			TArray<CResource> BuildingOutputMap;
			auto Lambda = [](TPair<FName, int> Input)
			{
				return CResource(CEntityName(Input.Key), Input.Value);
			};
			Algo::Transform(FactoryInput, BuildingInputMap, Lambda);
			Algo::Transform(FactoryOutput, BuildingOutputMap, Lambda);

			FactoryList.Add(CFactory(CEntityName(Context.Name), BuildingInputMap, BuildingOutputMap));
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreateMineListCommand() -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([](FEnttData& EnttData)
		{
			if (EnttData.ResourceIsCreated<FMineList>())
				return ESystemStatus::Failed;

			EnttData.CreateResource(FMineList());
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreateMineCommand(const FCreateMineInitContext& Context) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([Context](FEnttData& EnttData)
		{
			auto MineOutput = Context.MineOutput;
			auto& MineList = EnttData.GetResource<FMineList>();
			if (MineList.Contains(CEntityName(Context.Name))
				|| !IsCorrectResourceMap(MineOutput, EnttData))
				return ESystemStatus::Failed;

			TArray<CResource> BuildingOutputMap;
			auto Lambda = [](TPair<FName, int> Input)
			{
				return CResource(CEntityName(Input.Key), Input.Value);
			};
			Algo::Transform(MineOutput, BuildingOutputMap, Lambda);

			MineList.Add(CMine(CEntityName(Context.Name), BuildingOutputMap));
			return ESystemStatus::Break;
		});
	}

	//Work with entity

	FORCEINLINE auto GetCreateWorldCommand() -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([](FEnttData& EnttData)
		{
			auto Vi = EnttData.GetView(EWorld{});
			if (Vi.begin() != Vi.end())
				return ESystemStatus::Failed;

			const auto Entity = EnttData.Create(EWorld{});
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreateHexCommand() -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([](FEnttData& EnttData)
		{
			if (!EnttData.ResourceIsCreated<FGameRules>())
			{
				return ESystemStatus::Failed;
			}
			auto Vi = EnttData.GetView(entt::type_list<CHexTag>{});
			if (Vi.begin() != Vi.end())
				return ESystemStatus::Failed;

			const auto& GameRules = EnttData.GetResource<FGameRules>();
			const auto Length = GameRules.MapLength;
			const auto Height = GameRules.MapHeight;

			for (uint32 Y = 0; Y < Height; ++Y)
				for (uint32 X = 0; X < Length; ++X)
				{
					const auto Entity = EnttData.Create(EHex{});
					auto [EntityName, CubeCoordinate] = EnttData.Registry.get<
						CEntityName, CCubeCoordinate>(Entity);

					EntityName =
						CEntityName(FName(TEXT("Hex_") + FString::FromInt(X) + TEXT("_") + FString::FromInt(Y)));
					CubeCoordinate = CCubeCoordinate(ConvertToCubeCoordinate(FIntVector(X, Y, 0)));
				}

			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreateMainMap(UWorld* UEWorld) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([UEWorld](FEnttData& EnttData)
		{
			if (!EnttData.ResourceIsCreated<FRenderRules>())
				return ESystemStatus::Failed;

			const FRenderRules& RenderRules = EnttData.GetResource<FRenderRules>();
			TArray<FVector> Vertices;
			TArray<int32> Triangles;
			auto HexVi = std::as_const(EnttData).GetView(
				entt::type_list<CHexTag, CCubeCoordinate, CHexData>{});
			HexVi.each([&RenderRules, &Vertices, &Triangles](auto Entity, const auto& Coordinate, const auto& HexData)
			{
				const FVector Center = RenderRules.ConvertToRealCoordinate(Coordinate.Value);
				for (int32 Index = 0; Index < 6; Index++)
				{
					int32 VertexIndex = Vertices.Num();
					Vertices.Add(Center);
					Vertices.Add(Center + RenderRules.Corners[Index]);
					Vertices.Add(Center + RenderRules.Corners[Index + 1]);
					Triangles.Add(VertexIndex);
					Triangles.Add(VertexIndex + 1);
					Triangles.Add(VertexIndex + 2);
				}
			});

			const FVector Location = FVector::ZeroVector;
			const FRotator Rotation = FRotator::ZeroRotator;
			const FActorSpawnParameters SpawnInfo;
			auto Controller = UEWorld->SpawnActor<AMapController>(AMapController::StaticClass(), Location,
			                                                      Rotation,
			                                                      SpawnInfo);

			const auto Entity = EnttData.Create(EMap{});
			auto& MapController = EnttData.Registry.get<CMapControllerPtr>(Entity);
			MapController = CMapControllerPtr(Controller);
			MapController.Value->Init(Vertices, Triangles);
			EnttData.Registry.emplace<CNeedToRefresh>(Entity);
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreatePlayerCommand(AMainPlayerController* Player) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([Player](FEnttData& EnttData)
		{
			const auto Entity = EnttData.Create(EPlayer{});
			auto& Controller = EnttData.Registry.get<CPlayerController>(Entity);
			Controller = CPlayerController(Player);
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetSetResourceAndCityInTheWorldCommand() -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([](FEnttData& EnttData)
		{
			const auto& ResourceList = EnttData.GetResource<FResourceList>();
			EnttData.GetView(entt::type_list<CHexTag, CHexData>{}).each(
				[&ResourceList, &EnttData](auto Entity, auto& HexData)
				{
					for (const auto& Res : ResourceList.Resources)
						HexData.AddResources(Res, 100);
				});

			const auto& MineList = EnttData.GetResource<FMineList>();
			const auto& FactoryList = EnttData.GetResource<FFactoryList>();
			const auto& CityEntity = EnttData.Create(ECity{});
			auto [CityData, CubeCoordinate, EntityName] = EnttData.Registry.get<
				CCityData, CCubeCoordinate, CEntityName>(CityEntity);
			CityData = CCityData(FactoryList.Factories, MineList.Mines);
			CubeCoordinate = CCubeCoordinate(FIntVector::ZeroValue);
			EntityName = CEntityName(FName(TEXT("Hoofington")));


			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetCreateUnitCommand(UWorld* UEWorld, const FCreateUnitInitContext& Context) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([UEWorld, Context](FEnttData& EnttData)
		{
			const FVector Location = FVector::ZeroVector;
			const FRotator Rotation = FRotator::ZeroRotator;
			const FActorSpawnParameters SpawnInfo;
			auto Controller = UEWorld->SpawnActor<AUnitController>(AUnitController::StaticClass(), Location,
			                                                       Rotation,
			                                                       SpawnInfo);
			const auto& RenderRules = EnttData.GetResource<FRenderRules>();
			const auto MeshLocation = RenderRules.ConvertToRealCoordinate(Context.Position);
			auto MeshTransform = FTransform(MeshLocation);
			MeshTransform.SetScale3D(FVector(0.1f));
			Controller->Init(Context.UnityMesh, Context.Material, MeshTransform);
			const auto Entity = EnttData.Create(EUnit{});
			auto [EntityName, CubeCoordinate, UnitData, UnitController] = EnttData.Registry.get<
				CEntityName, CCubeCoordinate, CUnitData, CUnitController>(Entity);
			EntityName = CEntityName(Context.Name);
			CubeCoordinate = CCubeCoordinate(Context.Position);
			UnitData = CUnitData(Context.MaxAp);
			UnitController = CUnitController(Controller);
			
			Controller->Move(RenderRules.ConvertToRealCoordinate(Context.Position));
			return ESystemStatus::Break;
		});
	}

	//GUI commands (rename)

	FORCEINLINE auto GetWorldInfoCommand(TScriptInterface<ITextViewerWidget> Widget) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([Widget](FEnttData& EnttData)
		{
			auto WorldVi = EnttData.GetView(EWorld{});
			if (WorldVi.begin() == WorldVi.end())
				return ESystemStatus::Failed;

			auto WorldId = *EnttData.GetView(EWorld{}).begin();
			FTextBuilder TextBuilder;
			EnttData.GetTextInfo(WorldId, TextBuilder, entt::type_list<CWorldInfo>{}, {});
			TextBuilder.AppendLine(FString(TEXT("Entities: ") + FString::FromInt(EnttData.Registry.alive())));
			TextBuilder.AppendLine(FString(TEXT("Resources: ") + FString::FromInt(EnttData.ResourcesAlive())));
			ITextViewerWidget::Execute_OnUpdateText(Widget.GetObject(), TextBuilder.ToText());
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetMapInfoCommand(TScriptInterface<ITextViewerWidget> Widget) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([Widget](FEnttData& EnttData)
		{
			if (!EnttData.ResourceIsCreated<FGameRules>())
				return ESystemStatus::Failed;

			FTextBuilder TextBuilder;
			EnttData.GetTextInfo<FGameRules>(TextBuilder);
			ITextViewerWidget::Execute_OnUpdateText(Widget.GetObject(), TextBuilder.ToText());
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetHexInfoCommand(TScriptInterface<ITextViewerWidget> Widget,
	                                   const FVector& Location) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([Widget, Location](FEnttData& EnttData)
		{
			if (!EnttData.ResourceIsCreated<FRenderRules>())
				return ESystemStatus::Failed;
			const auto& RenderRules = EnttData.GetResource<FRenderRules>();
			const auto CubeCoordinate = CCubeCoordinate(RenderRules.ConvertToCubeCoordinate(Location));

			auto HexVi = std::as_const(EnttData).GetView(entt::type_list<CHexTag, CCubeCoordinate>{});
			auto HexId = std::find_if(HexVi.begin(), HexVi.end(), TEquals(HexVi, CubeCoordinate));
			if (HexId == HexVi.end())
				return ESystemStatus::Failed;

			FTextBuilder TextBuilder;
			EnttData.GetTextInfo(
				*HexId, TextBuilder, entt::type_list<CEntityName, CCubeCoordinate, CHexData>{}, {});
			TextBuilder.AppendLine(FString(TEXT(" ")));

			auto CityVi = std::as_const(EnttData).GetView(entt::type_list<CCityTag, CCubeCoordinate>{});
			auto CityId = std::find_if(CityVi.begin(), CityVi.end(), TEquals(CityVi, CubeCoordinate));
			if (CityId == CityVi.end())
			{
				ITextViewerWidget::Execute_OnUpdateText(Widget.GetObject(), TextBuilder.ToText());
				return ESystemStatus::Break;
			}

			EnttData.GetTextInfo(
				*CityId, TextBuilder, entt::type_list<CEntityName, CCityData>{}, {});


			ITextViewerWidget::Execute_OnUpdateText(Widget.GetObject(), TextBuilder.ToText());
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetAllResourceInfoCommand(TScriptInterface<ITextViewerWidget> Widget) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([Widget](FEnttData& EnttData)
		{
			if (!EnttData.ResourceIsCreated<FResourceList>())
				return ESystemStatus::Failed;

			FTextBuilder TextBuilder;
			const auto& ResourceList = EnttData.GetResource<FResourceList>();
			ResourceList.GetTextInfo(TextBuilder);
			TextBuilder.AppendLine(FString(TEXT(" ")));
			ITextViewerWidget::Execute_OnUpdateText(Widget.GetObject(), TextBuilder.ToText());
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetAllBuildingInfoCommand(TScriptInterface<ITextViewerWidget> Widget) -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([Widget](FEnttData& EnttData)
		{
			if (!EnttData.ResourceIsCreated<FFactoryList>() || !EnttData.ResourceIsCreated<FMineList>())
				return ESystemStatus::Failed;
			FTextBuilder TextBuilder;
			auto& FactoryList = EnttData.GetResource<FFactoryList>();
			FactoryList.GetTextInfo(TextBuilder);
			TextBuilder.AppendLine(FString(TEXT(" ")));
			auto& MineList = EnttData.GetResource<FMineList>();
			MineList.GetTextInfo(TextBuilder);
			ITextViewerWidget::Execute_OnUpdateText(Widget.GetObject(), TextBuilder.ToText());
			return ESystemStatus::Break;
		});
	}

	FORCEINLINE auto GetIncrementingTurnCommand() -> decltype(auto)
	{
		return TFunction<ESystemStatus(FEnttData&)>([](FEnttData& EnttData)
		{
			auto Vi = EnttData.GetView(EWorld{});
			if (Vi.begin() == Vi.end())
				return ESystemStatus::Failed;

			auto WorldId = *EnttData.GetView(EWorld{}).begin();
			EnttData.Registry.emplace_or_replace<CNextTurn>(WorldId);
			return ESystemStatus::Break;
		});
	}
}
