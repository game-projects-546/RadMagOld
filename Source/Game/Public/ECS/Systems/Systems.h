// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Commands.h"
#include "Scene/MainPlayerController.h"

namespace RadMag
{
	//Turn system

	FORCEINLINE auto UpdateCity(FEnttData& EnttData, float DeltaTime) -> ESystemStatus
	{
		EnttData.GetView(entt::type_list<CCityTag, CCityData>{}).each(
			[](auto Entity, CCityData& CityData)
			{
				auto& Storage = CityData.Resources;
				for (auto& Element : CityData.Factories)
				{
					const auto InputResCont = CityData.ResourceContains(Element.BuildingInput);
					if (!InputResCont)
						continue;
					CityData.DeleteResources(Element.BuildingInput);
					CityData.AddResources(Element.BuildingOutput);
				}
				for (auto& Element : CityData.Mines)
				{
					CityData.AddResources(Element.BuildingOutput);
				}
			});
		return ESystemStatus::Repeat;
	}

	FORCEINLINE auto UpdateUnit(FEnttData& EnttData, float DeltaTime) -> ESystemStatus
	{
		EnttData.GetView(entt::type_list<CUnitTag, CUnitData>{}).each(
			[](auto Entity, CUnitData& UnitData)
			{
				UnitData.AP = UnitData.MaxAP;
			});
		return ESystemStatus::Repeat;
	}

	FORCEINLINE auto NextTurn(FEnttData& EnttData, float DeltaTime) -> ESystemStatus
	{
		auto WorldVi = EnttData.GetView(entt::type_list<CWorld, CNextTurn, CWorldInfo>{});
		if (WorldVi.begin() == WorldVi.end())
			return ESystemStatus::Repeat;

		auto WorldId = *WorldVi.begin();
		auto& WorldInfo = WorldVi.get<CWorldInfo>(WorldId);
		++WorldInfo.CurrentTurn;

		auto MapVi = EnttData.GetView(entt::type_list<CMapTag>{});
		MapVi.each(
			[&EnttData](auto Entity)
			{
				EnttData.Registry.emplace<CNeedToRefresh>(Entity);
			});

		auto PlayerVi = EnttData.GetView(entt::type_list<CPlayerTag, CPlayerController>{});
		PlayerVi.each(
			[&EnttData](auto Entity, auto& PlayerController)
			{
				IListenerOfNextTurn::Execute_OnNextTurn(PlayerController.Value);
			});

		EnttData.Registry.remove<CNextTurn>(WorldId);
		UpdateCity(EnttData, DeltaTime);
		UpdateUnit(EnttData, DeltaTime);
		return ESystemStatus::Repeat;
	}


	//
	FORCEINLINE auto ParseInput(FEnttData& EnttData, float DeltaTime) -> ESystemStatus
	{
		auto PlayerVi = EnttData.GetView(entt::type_list<CPlayerTag, CPlayerController>{});
		if (PlayerVi.begin() == PlayerVi.end())
			return ESystemStatus::Repeat;

		auto PlayerId = *PlayerVi.begin();
		auto [Entity, PlayerController] = *PlayerVi.each().begin();
		TOptional<CMouseEvent> EventOptional = PlayerController.Value->GetMouseEvent();
		if (!EventOptional.IsSet())
			return ESystemStatus::Repeat;

		CMouseEvent Event = EventOptional.Get(CMouseEvent());
		const auto& RenderRules = EnttData.GetResource<FRenderRules>();
		if (Event.Key == EKeys::LeftMouseButton)
		{
			if (EnttData.Registry.any_of<CDedicatedUnit>(PlayerId))
			{
				auto UnitId = EnttData.Registry.get<CDedicatedUnit>(PlayerId).Value;
				EnttData.Registry.emplace_or_replace<CMoveOrder>(
					UnitId, CCubeCoordinate(RenderRules.ConvertToCubeCoordinate(Event.Location)));
			}
		}
		if (Event.Key == EKeys::RightMouseButton)
		{
			auto UnitVi = std::as_const(EnttData).GetView(entt::type_list<CUnitTag, CCubeCoordinate>{});
			auto It = std::find_if(UnitVi.begin(), UnitVi.end(),
			                       TEquals(UnitVi, CCubeCoordinate(
				                               RenderRules.ConvertToCubeCoordinate(Event.Location))));
			if(It == UnitVi.end())
			{
				if(EnttData.Registry.any_of<CDedicatedUnit>(PlayerId))
					EnttData.Registry.remove<CDedicatedUnit>(PlayerId);
				return ESystemStatus::Repeat;
			}

			const auto UnitId = *It;
			EnttData.Registry.emplace_or_replace<CDedicatedUnit>(PlayerId, CDedicatedUnit(UnitId));			
		}

		return ESystemStatus::Repeat;
	}

	FORCEINLINE auto MoveUnit(FEnttData& EnttData, float DeltaTime) -> ESystemStatus
	{
		EnttData.GetView(entt::type_list<CUnitTag, CCubeCoordinate, CUnitData, CMoveOrder, CUnitController>{}).each(
			[&EnttData](auto Entity, auto& CubeCoordinate, auto& UnitData, auto& MoveOrder, auto& UnitController)
			{
				auto NeedAP = FMath::CeilToFloat(CCubeCoordinate::Distance(CubeCoordinate, MoveOrder.Value));
				if(UnitData.AP > NeedAP)
				{
					UnitData.AP -= NeedAP;
					CubeCoordinate = MoveOrder.Value;
					const auto& RenderRules = EnttData.GetResource<FRenderRules>();
					UnitController.Value->Move(RenderRules.ConvertToRealCoordinate(CubeCoordinate.Value));
				}
				EnttData.Registry.remove<CMoveOrder>(Entity);
			});
			
		

		return ESystemStatus::Repeat;
	}
}
