// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "ECS/Components/Components.h"
#include "entt/entt.hpp"

class AUnitController;
class AMainPlayerController;
class AMapController;

namespace RadMag
{
	using CWorld = entt::tag<entt::hashed_string("EWorld")>;
	using CNextTurn = entt::tag<entt::hashed_string("NextTurn")>;
	using EWorld = entt::type_list<CWorld, CWorldInfo>::type;
	
	using CMapTag = entt::tag<entt::hashed_string("EMap")>;
	using CMapControllerPtr = CActorPtr<AMapController>;
	using CNeedToRefresh = entt::tag<entt::hashed_string("NeedToRefresh")>;
	using EMap = entt::type_list<CMapTag, CMapControllerPtr>::type;
	
	using CPlayerTag = entt::tag<entt::hashed_string("EPlayer")>;
	using CPlayerController = CActorPtr<AMainPlayerController>;
	using EPlayer = entt::type_list<CPlayerTag, CPlayerController>::type;
	
	using CHexTag = entt::tag<entt::hashed_string("Hex")>;
	using EHex = entt::type_list<CHexTag, CEntityName, CCubeCoordinate, CHexData>::type;	

	using CCityTag = entt::tag<entt::hashed_string("ECity")>;
	using ECity = entt::type_list<CCityTag, CEntityName, CCubeCoordinate, CCityData>::type;

	using CUnitTag = entt::tag<entt::hashed_string("EUnit")>;
	using CUnitController = CActorPtr<AUnitController>;
	using EUnit = entt::type_list<CUnitTag, CEntityName, CCubeCoordinate, CUnitData, CUnitController>::type;
}
