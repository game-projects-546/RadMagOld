// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

namespace RadMag
{
	FORCEINLINE auto ConvertToCubeCoordinate(const FIntVector& OffsetCoordinate) -> FIntVector
	{
		const auto X = OffsetCoordinate.X - (OffsetCoordinate.Y - (OffsetCoordinate.Y & 1)) / 2;
		const auto Z = OffsetCoordinate.Y;
		const auto Y = -X - Z;
		return FIntVector(X, Y, Z);
	}

	FORCEINLINE auto ConvertToOffsetCoordinate(const FIntVector& CubeCoordinate) -> FIntVector
	{
		const auto Col = CubeCoordinate.X + (CubeCoordinate.Z - (CubeCoordinate.Z & 1)) / 2;
		const auto Row = CubeCoordinate.Z;
		return FIntVector(Col, Row, 0);
	}
}
