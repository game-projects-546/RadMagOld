﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "entt/entt.hpp"

namespace RadMag
{
	//Operation with type list
	template <typename T>
	struct TAddConst
	{
		using Type = T const;
	};

	template <template<typename T> class MetaFun, typename... Elements>
	struct TTransform;

	template <template<typename T> class MetaFun, typename... Elements>
	struct TTransform<MetaFun, entt::type_list<Elements...>>
	{
		using Type = typename entt::type_list<typename MetaFun<Elements>::Type...>::type;
	};

	//Basic function for work with View and algorithms c++

	template <typename View, typename T>
	struct TEquals
	{
	private:
		View Vi;
		T Object;		

	public:

		TEquals() = delete;

		TEquals(View Vi, T Object)
			: Vi(Vi), Object(Object)
		{
		}

		template <typename Iterator>
		auto operator()(Iterator It) const -> bool
		{
			return Object == Vi.template get<T const>(It);
		}
	};

	//Get Info on entity
	
}
