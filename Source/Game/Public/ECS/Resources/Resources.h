// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "ECS/Components/Components.h"
#include "ECS/Others/GameMath.h"

//You must add void GetTextInfo(FTextBuilder&) method for all resources
namespace RadMag
{
	struct FGameRules
	{
		uint32 MapLength;
		uint32 MapHeight;

		FGameRules() = default;

		FGameRules(uint32 Length, uint32 Height)
			: MapLength(Length),
			  MapHeight(Height)
		{
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(FString(TEXT("GameRules")));
			TextBuilder.AppendLine(FString(TEXT("Map length: ")) + FString::FromInt(MapLength));
			TextBuilder.AppendLine(FString(TEXT("Map height: ")) + FString::FromInt(MapHeight));
			TextBuilder.AppendLine(FString(TEXT(" ")));
		}
	};

	struct FRenderRules
	{
		float OuterRadius = 10.f;
		float OuterToInner = 0.866025404f;
		float InnerRadius = OuterRadius * OuterToInner;
		TArray<FVector> Corners{
			FVector(0.f, OuterRadius, 0.f),
			FVector(InnerRadius, 0.5f * OuterRadius, 0.f),
			FVector(InnerRadius, -0.5f * OuterRadius, 0.f),
			FVector(0.f, -OuterRadius, 0.f),
			FVector(-InnerRadius, -0.5f * OuterRadius, 0.f),
			FVector(-InnerRadius, 0.5f * OuterRadius, 0.f),
			FVector(0.f, OuterRadius, 0.f)
		};
		TMap<FName, FColor> BiomeConvertor{
			MakeTuple(FName(TEXT("None")), FColor::Black),
			MakeTuple(FName(TEXT("Ocean")), FColor::Blue),
			MakeTuple(FName(TEXT("Desert")), FColor::Yellow)
		};

		auto GetInnerDiameter() const -> float
		{
			return 2.f * OuterRadius * OuterToInner;
		}

		auto ConvertToRealCoordinate(const FIntVector& CubeCoordinate) const -> decltype(auto)
		{
			const auto OffsetCoordinate = ConvertToOffsetCoordinate(CubeCoordinate);
			const auto X = (OffsetCoordinate.X + OffsetCoordinate.Y * 0.5f - OffsetCoordinate.Y / 2) *
				GetInnerDiameter();
			const auto Y = OffsetCoordinate.Y * (OuterRadius * 1.5f);
			const auto Z = 0.f;
			return FVector(X, Y, Z);
		}

		auto ConvertToCubeCoordinate(const FVector& RealCoordinate) const -> decltype(auto)
		{
			float X = RealCoordinate.X / GetInnerDiameter();
			float Y = -X;
			const float Offset = RealCoordinate.Y / (OuterRadius * 3.0f);
			X -= Offset;
			Y -= Offset;
			int32 iX = FMath::RoundToInt(X);
			int32 iY = FMath::RoundToInt(Y);
			int32 iZ = FMath::RoundToInt(-X - Y);
			if (iX + iY + iZ != 0)
			{
				const float dX = FMath::RoundToInt(X - iX);
				const float dY = FMath::RoundToInt(Y - iY);
				const float dZ = FMath::RoundToInt(-X - Y - iZ);
				if (dX > dY && dX > dZ)
					iX = -iY - iZ;
				else if (dZ > dY)
					iZ = -iX - iY;
			}
			iY = -iX - iZ;
			return FIntVector(iX, iY, iZ);
		}

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(FString(TEXT("RenderRules")));
			TextBuilder.AppendLine(FString(TEXT(" ")));
		}
	};

	struct FResourceList
	{
		TArray<CResource> Resources;

		FResourceList() = default;

		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{			
			TextBuilder.AppendLine(FString(TEXT("Resource list")));
			for (auto& Resource : Resources)
				TextBuilder.AppendLine(Resource.Name.Value.ToString());
		}

		auto Add(CResource Resource)
		{
			Resources.Add(Resource);
		}

		auto Contains(const CEntityName& Name) const -> decltype(auto)
		{
			for(auto Res : Resources)
				if(Res.Name == Name)
					return true;
			return false;
		}
	};

	struct FFactoryList
	{
		TArray<CFactory> Factories;

		FFactoryList() = default;
		
		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(FString(TEXT("Factory list")));			
			for (auto& Factory : Factories)
				Factory.GetTextInfo(TextBuilder);
		}

		auto Add(CFactory Factory)
		{
			Factories.Add(Factory);
		}

		auto Contains(const CEntityName& Name) const -> decltype(auto)
		{
			for(auto Factory : Factories)
				if(Factory.Name == Name)
					return true;
			return false;
		}
	};

	struct FMineList
	{
		TArray<CMine> Mines;

		FMineList() = default;
		
		auto GetTextInfo(FTextBuilder& TextBuilder) const -> void
		{
			TextBuilder.AppendLine(FString(TEXT("Mine list")));			
			for (auto& Element : Mines)
				Element.GetTextInfo(TextBuilder);
		}

		auto Add(CMine Mine)
		{
			Mines.Add(Mine);
		}

		auto Contains(const CEntityName& Name) const -> decltype(auto)
		{
			for(auto Element : Mines)
				if(Element.Name == Name)
					return true;
			return false;
		}
	};
}
