// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Engine/StaticMesh.h"
#include "InitContext.generated.h"

USTRUCT(BlueprintType)
struct FCreateGameRulesInitContext
{
	GENERATED_BODY()

	UPROPERTY(EditInstanceOnly)
	int32 MapLength;
	UPROPERTY(EditInstanceOnly)
	int32 MapHeight;
};

USTRUCT(BlueprintType)
struct FCreateResourceInitContext
{
	GENERATED_BODY()

	UPROPERTY(EditInstanceOnly)
	FName Name;
};

USTRUCT(BlueprintType)
struct FCreateFactoryInitContext
{
	GENERATED_BODY()
	
	UPROPERTY(EditInstanceOnly)
	FName Name;
	UPROPERTY(EditInstanceOnly)
	TMap<FName, int32> FactoryInput;
	UPROPERTY(EditInstanceOnly)
	TMap<FName, int32> FactoryOutput;
};

USTRUCT(BlueprintType)
struct FCreateMineInitContext
{
	GENERATED_BODY()
	
	UPROPERTY(EditInstanceOnly)
	FName Name;
	UPROPERTY(EditInstanceOnly)
	TMap<FName, int32> MineOutput;
};

USTRUCT(BlueprintType)
struct FCreateUnitInitContext
{
	GENERATED_BODY()
	
	UPROPERTY(EditInstanceOnly)
	FName Name;	
	UPROPERTY(EditInstanceOnly)
	FIntVector Position;
	UPROPERTY(EditInstanceOnly)
	UStaticMesh* UnityMesh;
	UPROPERTY(EditInstanceOnly)
	UMaterialInstance* Material;
	UPROPERTY(EditInstanceOnly)
	int32 MaxAp;
	
};
