// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "EnttIntegration/Modules/EnttModuleBase.h"
#include "GameFramework/GameModeBase.h"
#include "MainGameMode.generated.h"

class UGameData;
/**
 * 
 */
UCLASS(Abstract)
class GAME_API AMainGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditDefaultsOnly)
	TArray<TSubclassOf<UEnttModuleBase>> EnttModules;

	UPROPERTY(EditDefaultsOnly)
	UGameData* WorldData;

protected:
	virtual void BeginPlay() override;
	void Bootstrap();
};
