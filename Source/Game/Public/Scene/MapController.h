// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "MapController.generated.h"

UCLASS()
class GAME_API AMapController : public AActor
{
	GENERATED_BODY()
private:

	bool bInit;

	UPROPERTY()
	UProceduralMeshComponent* Mesh;

	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FColor> VertexColors;

public:
	AMapController();

protected:

	virtual void BeginPlay() override;

public:
	void Init(const TArray<FVector>& SetVertices, const TArray<int32>& SetTriangles);
};
