﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "WorldDataSet.generated.h"

class UGameData;
UINTERFACE(MinimalAPI)
class UWorldDataSet : public UInterface
{
	GENERATED_BODY()
};

class GAME_API IWorldDataSet
{
	GENERATED_BODY()
	
public:

	virtual void SetWorldData(UGameData* SetWorldData) = 0;
};
