// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnttIntegration/Modules/EnttModuleBase.h"
#include "EnttIntegration/Modules/ModuleInterface/ECSWorldSet.h"
#include "EnttIntegration/Modules/ModuleInterface/UEWorldSet.h"
#include "ModuleInterface/WorldDataSet.h"
#include "CreationSystemsModule.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API UCreationSystemsModule : public UEnttModuleBase, public IUEWorldSet, public IECSWorldSet,
                                        public IWorldDataSet
{
	GENERATED_BODY()

private:

	RadMag::FEnttWorld* EnttWorldPtr;

	UPROPERTY()
	UWorld* UEWorld;

	UPROPERTY()
	UGameData* GameData;

public:
	virtual void SetECSWorld(RadMag::FEnttWorld* SetWorld) override;
	virtual void SetUEWorld(UWorld* SetWorld) override;
	virtual void SetWorldData(UGameData* SetWorldData) override;
	
	virtual void Initialize() override;
};
