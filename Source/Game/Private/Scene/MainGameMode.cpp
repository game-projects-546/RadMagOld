// Fill out your copyright notice in the Description page of Project Settings.


#include "Scene/MainGameMode.h"
#include "Engine/World.h"
#include "EnttIntegration/UnrealEnttSubsystem.h"
#include "EnttIntegration/Modules/ModuleInterface/ECSWorldSet.h"
#include "EnttIntegration/Modules/ModuleInterface/UEWorldSet.h"
#include "Modules/ModuleInterface/WorldDataSet.h"

void AMainGameMode::BeginPlay()
{
	Super::BeginPlay();
	Bootstrap();
}

void AMainGameMode::Bootstrap()
{
	const auto GameInstance = GetWorld()->GetGameInstance();
	const auto ECS = GameInstance->GetSubsystem<UUnrealEnttSubsystem>();

	for(auto ModuleClass : EnttModules)
	{
		auto Module = NewObject<UEnttModuleBase>(this, ModuleClass);
		if(Module->Implements<UECSWorldSet>())
		{
			Cast<IECSWorldSet>(Module)->SetECSWorld(ECS->GetECSWorld());
		}
		if(Module->Implements<UUEWorldSet>())
		{
			Cast<IUEWorldSet>(Module)->SetUEWorld(GetWorld());
		}
		if(Module->Implements<UWorldDataSet>())
		{
			Cast<IWorldDataSet>(Module)->SetWorldData(WorldData);
		}
		Module->Initialize();
	}
}
