// Fill out your copyright notice in the Description page of Project Settings.


#include "Scene/UnitController.h"

AUnitController::AUnitController()
{
	PrimaryActorTick.bCanEverTick = false;
	bInit = false;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	SetRootComponent(MeshComponent);
}

void AUnitController::Init(UStaticMesh* SetMesh, UMaterialInstance* SetMaterial, const FTransform& Transform)
{
	check(!bInit);
	MeshComponent->SetStaticMesh(SetMesh);
	MeshComponent->SetMaterial(0, SetMaterial);
	MeshComponent->CastShadow = false;
	MeshComponent->bCastDynamicShadow = false;
	MeshComponent->bCastStaticShadow = false;
	MeshComponent->bUseDefaultCollision = false;
	MeshComponent->SetGenerateOverlapEvents(false);
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComponent->SetCanEverAffectNavigation(false);
	SetActorTransform(Transform);
	bInit = true;
}

void AUnitController::Move(FVector NewLocation)
{
	check(bInit);
	SetActorLocation(NewLocation);
}
