// Fill out your copyright notice in the Description page of Project Settings.


#include "Scene/MapController.h"


// Sets default values
AMapController::AMapController()
{
	PrimaryActorTick.bCanEverTick = false;
	bInit = false;
	Mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Mesh"));
}

void AMapController::BeginPlay()
{
	Super::BeginPlay();
	
}

void AMapController::Init(const TArray<FVector>& SetVertices, const TArray<int32>& SetTriangles)
{
	check(!bInit);
	Vertices = SetVertices;
	Triangles = SetTriangles;
	Mesh->CreateMeshSection(0, Vertices, Triangles, TArray<FVector>(), TArray<FVector2D>(),
                            VertexColors, TArray<FProcMeshTangent>(), true);
	bInit = true;
}

