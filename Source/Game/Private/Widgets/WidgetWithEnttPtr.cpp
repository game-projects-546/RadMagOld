﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "Widgets/WidgetWithEnttPtr.h"


#include "ECS/Systems/Commands.h"
#include "EnttIntegration/UnrealEnttSubsystem.h"

void UWidgetWithEnttPtr::Init()
{
	const auto Player = GetOwningPlayer();
	check(Player);
	const auto GameInstance = Player->GetGameInstance();
	EnttWorld = GameInstance->GetSubsystem<UUnrealEnttSubsystem>()->GetECSWorld();
	check(EnttWorld);
}

void UWidgetWithEnttPtr::GetWorldInfo(TScriptInterface<ITextViewerWidget> Widget)
{
	const auto Status = EnttWorld->ExecuteSystem(RadMag::GetWorldInfoCommand(Widget));
	check(Status != RadMag::ESystemStatus::Failed);
}

void UWidgetWithEnttPtr::GetMapInfo(TScriptInterface<ITextViewerWidget> Widget)
{
	const auto Status = EnttWorld->ExecuteSystem(RadMag::GetMapInfoCommand(Widget));
	check(Status != RadMag::ESystemStatus::Failed);
}

void UWidgetWithEnttPtr::GetHexInfo(TScriptInterface<ITextViewerWidget> Widget, const FVector& Location)
{
	const auto Status = EnttWorld->ExecuteSystem(RadMag::GetHexInfoCommand(Widget, Location));
	check(Status != RadMag::ESystemStatus::Failed);
}

void UWidgetWithEnttPtr::GetAllResourceInfo(TScriptInterface<ITextViewerWidget> Widget)
{
	const auto Status = EnttWorld->ExecuteSystem(RadMag::GetAllResourceInfoCommand(Widget));
	check(Status != RadMag::ESystemStatus::Failed);
}

void UWidgetWithEnttPtr::GetAllBuildingInfo(TScriptInterface<ITextViewerWidget> Widget)
{
	const auto Status = EnttWorld->ExecuteSystem(RadMag::GetAllBuildingInfoCommand(Widget));
	check(Status != RadMag::ESystemStatus::Failed);
}

void UWidgetWithEnttPtr::NextTurn()
{
	const auto Status = EnttWorld->ExecuteSystem(RadMag::GetIncrementingTurnCommand());
	check(Status != RadMag::ESystemStatus::Failed);
}

