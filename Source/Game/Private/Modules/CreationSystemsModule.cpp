// Fill out your copyright notice in the Description page of Project Settings.


#include "Modules/CreationSystemsModule.h"
#include "Data/GameData.h"
#include "ECS/Systems/Commands.h"
#include "Kismet/GameplayStatics.h"
#include "Scene/MainPlayerController.h"

void UCreationSystemsModule::SetECSWorld(RadMag::FEnttWorld* SetWorld)
{
	EnttWorldPtr = SetWorld;
}

void UCreationSystemsModule::SetUEWorld(UWorld* SetWorld)
{
	UEWorld = SetWorld;
}

void UCreationSystemsModule::SetWorldData(UGameData* SetWorldData)
{
	GameData = SetWorldData;
}

void UCreationSystemsModule::Initialize()
{
	check(EnttWorldPtr);
	check(UEWorld);
	check(GameData);

	auto State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateGameRulesCommand(GameData->GameRules));
	check(State != RadMag::ESystemStatus::Failed);

	State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateRenderRulesCommand());
	check(State != RadMag::ESystemStatus::Failed);

	State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateWorldCommand());
	check(State != RadMag::ESystemStatus::Failed);

	State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateResourceListCommand());
	check(State != RadMag::ESystemStatus::Failed);
	for (auto& Element : GameData->Resources)
	{
		State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateResourceCommand(Element));
		check(State != RadMag::ESystemStatus::Failed);
	}

	State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateFactoryListCommand());
	check(State != RadMag::ESystemStatus::Failed);
	for (auto& Element : GameData->Factories)
	{
		State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateFactoryCommand(Element));
		check(State != RadMag::ESystemStatus::Failed);
	}

	State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateMineListCommand());
	check(State != RadMag::ESystemStatus::Failed);
	for (auto& Element : GameData->Mines)
	{
		State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateMineCommand(Element));
		check(State != RadMag::ESystemStatus::Failed);
	}

	State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateHexCommand());
	check(State != RadMag::ESystemStatus::Failed);

	State = EnttWorldPtr->ExecuteSystem(RadMag::GetSetResourceAndCityInTheWorldCommand());
	check(State != RadMag::ESystemStatus::Failed);

	State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateMainMap(UEWorld));
	check(State != RadMag::ESystemStatus::Failed);

	State = EnttWorldPtr->ExecuteSystem(
		RadMag::GetCreatePlayerCommand(
			Cast<AMainPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0))));
	check(State != RadMag::ESystemStatus::Failed);

	State = EnttWorldPtr->ExecuteSystem(RadMag::GetCreateUnitCommand(GetWorld(), GameData->Unit));
	check(State != RadMag::ESystemStatus::Failed);
}
